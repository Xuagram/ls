/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   slnk_dir.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/16 15:47:34 by temehenn          #+#    #+#             */
/*   Updated: 2019/08/20 11:09:02 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int	is_slnk_dir(char *name, struct stat lnk, int opt)
{
	struct stat check;

	if (S_ISLNK(lnk.st_mode) && !is_opt(opt, L_OPT))
	{
		stat(name, &check);
		if (S_ISDIR(check.st_mode))
			return (1);
	}
	return (0);
}
