/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ls_l.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/14 18:28:00 by teddy             #+#    #+#             */
/*   Updated: 2019/08/20 11:07:15 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

char	*get_uid(uid_t uid)
{
	struct passwd *pw;

	if (!(pw = getpwuid(uid)))
		return (0);
	return (pw->pw_name);
}

char	*get_gid(gid_t gid)
{
	struct group *gr;

	if (!(gr = getgrgid(gid)))
		return (0);
	return (gr->gr_name);
}

void	get_year(char str[26], char tmp[26], char result[13], time_t time)
{
	if (time > 253402297140)
		ft_printf("Jan  1  10000 ");
	else
	{
		ft_memmove(str, str + 4, 6);
		str[6] = '\0';
		ft_memmove(tmp, tmp + 18, 6);
		tmp[0] = ' ';
		tmp[6] = '\0';
		ft_strcat(ft_strcat(result, str), tmp);
		ft_printf("%s ", result);
	}
}

int		get_time(char *str, struct stat data)
{
	char	tmp[26];
	char	result[14];
	time_t	current;

	ft_bzero(tmp, 26 * sizeof(char));
	ft_bzero(result, 14 * sizeof(char));
	ft_strcpy(tmp, str);
	if (time(&current) == -1)
		return (-1);
	if ((current - data.st_mtime) > 15768017
			|| (current - data.st_mtime) <= -15778800)
		get_year(str, tmp, result, data.st_mtime);
	else
	{
		ft_memmove(str, str + 4, 12);
		str[12] = '\0';
		ft_printf("%s ", str);
	}
	return (0);
}

int		get_slnk(char *path, struct stat data)
{
	char	buf[256];
	size_t	bufsize;
	int		nbytes;

	ft_bzero(buf, 256);
	bufsize = data.st_size + 1;
	if (data.st_size == 0)
		bufsize = PATH_MAX;
	if ((nbytes = readlink(path, buf, bufsize)) == -1)
		return (-1);
	ft_printf(" -> %s", buf);
	return (0);
}
