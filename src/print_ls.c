/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_ls.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/14 18:06:11 by teddy             #+#    #+#             */
/*   Updated: 2019/08/19 12:39:14 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** print_space gets the exact size of the padding to put after each
** entry of ls-l. It just calculates the difference between the
** actual size of the entry and the maximum size of the entry of the
** same kind that is encountered for all the files.
*/

void	print_space(unsigned int size_max, unsigned int size_name)
{
	unsigned int space;

	space = size_max - size_name;
	while (space--)
		ft_putchar(' ');
}

/*
** ls_l is the function that displays every information of each file:
** the nature of the file, its permissions, the user identifier, the
** group identifier, the major and the minor if the file is a block
** file or a character device file, the date of the last modification,
** its name and the file it is linked to if it is a symbolic link file.
*/

int		ls_l(t_file *ptr, long int tab[9][2], unsigned int size_max[8])
{
	rights(tab, ptr->data);
	print_space(size_max[1], get_size(ptr->data.st_nlink));
	ft_printf("%u ", ptr->data.st_nlink);
	ft_printf("%s  ", ptr->uid);
	print_space(size_max[2], ft_strlen(ptr->uid));
	ft_printf("%s", ptr->gid);
	print_space(size_max[3], ft_strlen(ptr->gid));
	if (S_ISBLK(ptr->data.st_mode) || S_ISCHR(ptr->data.st_mode))
		put_maj_min(ptr->data.st_rdev, size_max);
	else
		put_space_size(ptr->data.st_size, size_max);
	if (get_time(ptr->date, ptr->data) == -1)
		return (-1);
	ft_printf("%s", ptr->d_name);
	if (S_ISLNK(ptr->data.st_mode))
		if (get_slnk(ptr->path, ptr->data) == -1)
			return (-1);
	ft_printf("\n");
	return (0);
}

/*
** If the l option is present, ls_la manages the displays the file.
** It handle the a option, printing the hidden files which names
** start with '.' if it is present.
*/

int		ls_la(t_list *file, long int opt, long int tab[9][2],
		unsigned int size_max[6])
{
	t_list	*tmp;

	tmp = file;
	ft_printf("total %d\n", size_max[5]);
	while (file)
	{
		if (!is_opt(opt, A_OPT))
		{
			if (((t_file *)file->content)->d_name[0] != '.')
				if (ls_l(file->content, tab, size_max) == -1)
					return (-1);
		}
		else
		{
			if (ls_l(file->content, tab, size_max) == -1)
				return (-1);
		}
		file = file->next;
	}
	file = tmp;
	return (0);
}

/*
** ls_a displays all the files of the given list (file list or directory list).
** Even the hidden files are printed.
*/

void	ls_a(t_list *file)
{
	t_list	*tmp;

	tmp = file;
	while (tmp)
	{
		ft_printf("%s\n", ((t_file *)tmp->content)->d_name);
		tmp = tmp->next;
	}
}

/*
** ls displays all the files of the given list (file list or directory list).
** However, the hidden files which names start with '.' are not displayed.
*/

void	ls(t_list *file)
{
	t_list	*tmp;

	tmp = file;
	while (tmp)
	{
		if (((t_file *)tmp->content)->d_name[0] != '.')
			ft_printf("%s\n", ((t_file *)tmp->content)->d_name);
		tmp = tmp->next;
	}
}
