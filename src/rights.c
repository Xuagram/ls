/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rights.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/18 11:58:11 by mahaffne          #+#    #+#             */
/*   Updated: 2019/08/20 11:08:04 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** init_rights initializes the array that will be used as a reference.
** Each mask that define a kind a right is associated with the
** relevant letter that will be printed in the ls -l.
*/

void			init_rights(long int tab[9][2])
{
	ft_bzero(tab, 9 * 2 * sizeof(long int));
	tab[0][0] = S_IRUSR;
	tab[0][1] = 'r';
	tab[1][0] = S_IWUSR;
	tab[1][1] = 'w';
	tab[2][0] = S_IXUSR;
	tab[2][1] = 'x';
	tab[3][0] = S_IRGRP;
	tab[3][1] = 'r';
	tab[4][0] = S_IWGRP;
	tab[4][1] = 'w';
	tab[5][0] = S_IXGRP;
	tab[5][1] = 'x';
	tab[6][0] = S_IROTH;
	tab[6][1] = 'r';
	tab[7][0] = S_IWOTH;
	tab[7][1] = 'w';
	tab[8][0] = S_IXOTH;
	tab[8][1] = 'x';
}

/*
** get_special handles the special permissions regarding the file.
** It checks with the corresponding mask if there is any setuid,
** getuid or sticky bit right.
*/

static void		get_special(struct stat sb, char rights[11])
{
	if (sb.st_mode & S_ISUID)
		rights[3] = (sb.st_mode & S_IXUSR) ? 's' : 'S';
	if (sb.st_mode & S_ISGID)
		rights[6] = (sb.st_mode & S_IXGRP) ? 's' : 'S';
	if (sb.st_mode & S_ISVTX)
		rights[9] = (sb.st_mode & S_IXOTH) ? 't' : 'T';
}

/*
** get_fmt checks with the corresponding mask the format of the
** file, wether it is a regular file (-), a directory (d), a
** character device file (c), a pipe file (p), a symbolic link
** file (l) or a socket file (=).
*/

static void		get_fmt(char rights[11], struct stat sb)
{
	if (S_ISREG(sb.st_mode))
		rights[0] = '-';
	else if (S_ISDIR(sb.st_mode))
		rights[0] = 'd';
	else if (S_ISCHR(sb.st_mode))
		rights[0] = 'c';
	else if (S_ISBLK(sb.st_mode))
		rights[0] = 'b';
	else if (S_ISFIFO(sb.st_mode))
		rights[0] = 'p';
	else if (S_ISLNK(sb.st_mode))
		rights[0] = 'l';
	else if (S_ISSOCK(sb.st_mode))
		rights[0] = 's';
}

/*
** get_rights iterates through the array containing the macro, and
** sets the rights according to it.
*/

void			rights(long int tab[9][2], struct stat sb)
{
	int			i;
	int			j;
	char		rights[11];

	i = -1;
	j = 1;
	ft_bzero(rights, 11 * sizeof(char));
	get_fmt(rights, sb);
	while (++i < 9)
	{
		if ((sb.st_mode & tab[i][0]))
			rights[j] = tab[i][1];
		else
			rights[j] = '-';
		if ((sb.st_mode & S_ISUID) || (sb.st_mode & S_ISGID)
				|| (sb.st_mode & S_ISVTX))
			get_special(sb, rights);
		j++;
	}
	ft_printf("%s  ", rights);
}
