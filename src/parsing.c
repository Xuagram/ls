/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsing.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: haffner <haffner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/19 12:17:02 by mahaffne          #+#    #+#             */
/*   Updated: 2020/07/24 22:13:12 by haffner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int	is_arg(char **argv, int argc)
{
	int i;

	i = 1;
	if (!argv)
		return (-1);
	while (i < argc && ft_strncmp(argv[i], "--", 2) && argv[i][1]
		&& argv[i][0] == '-')
		i++;
	if (argv[i] && !ft_strcmp(argv[i], "--"))
		i++;
	return (i != argc) ? i : 0;
}

/*
** parsing calls the function that parse the option and manages
** the case where the option is invalid with an error message.
** Then it saves the option in a variable and finally calls the
** function that parse the arguments of ls.
*/

int			parsing(int argc, char **argv, t_all_list *all, t_all_list *arg)
{
	int		pos_arg;
	int		ret;
	char	bad_opt;

	all->opt = ft_getopt(argc, argv, "1lrRta", ls_option);
	ret = check_opt_error(argc, argv, "1lrRta", &bad_opt);
	if (ret == -2)
	{
		ft_putstr_fd("ls: illegal option -- ", 2);
		ft_putchar_fd(bad_opt, 2);
	}
	else if (ret == -1)
		ft_putstr_fd("ls: illegal option -- -", 2);
	if (ret == -2 || ret == -1)
	{
		ft_putstr_fd(
		"\nusage: ls [-Ralrt] [file ...]\n", 2);
		exit(EXIT_FAILURE);
	}
	arg->opt = all->opt;
	if ((pos_arg = is_arg(argv, argc)))
		if (parse_args(argc, argv, arg, pos_arg) == -1)
			return (-1);
	return (0);
}
