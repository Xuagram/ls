/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/14 16:33:57 by teddy             #+#    #+#             */
/*   Updated: 2019/08/16 15:54:42 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int	main(int argc, char **argv)
{
	t_all_list	all;
	t_all_list	arg;

	init_struct(&all, &arg);
	if (parsing(argc, argv, &all, &arg) < 0)
	{
		print_error(arg, NULL);
		free_arg(&arg);
		exit(EXIT_FAILURE);
	}
	if (arg.file || arg.dir || arg.error)
	{
		if (manage_args(all, arg) < 0)
			exit(EXIT_FAILURE);
	}
	else if (ft_ls(".", all) < 0)
	{
		print_error(all, NULL);
		free_all(&all);
		free_arg(&arg);
		exit(EXIT_FAILURE);
	}
	free_arg(&arg);
	exit(EXIT_SUCCESS);
	return (0);
}
