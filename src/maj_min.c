/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   maj_min.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/04 12:56:01 by mahaffne          #+#    #+#             */
/*   Updated: 2019/08/20 11:08:50 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

void	put_maj_min(dev_t dev, unsigned int size_max[8])
{
	print_space(size_max[6], get_size(major(dev)));
	ft_printf("   %ld, ", major(dev));
	print_space(size_max[7], get_size(minor(dev)));
	ft_printf("%ld ", minor(dev));
}

void	put_space_size(int size, unsigned int size_max[8])
{
	if (size_max[6])
	{
		print_space(size_max[6] + size_max[7] + 3, get_size(size));
		ft_printf("  %llu ", size);
	}
	else
	{
		print_space(size_max[4], get_size(size));
		ft_printf("  %llu ", size);
	}
}
