/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/04 19:33:40 by teddy             #+#    #+#             */
/*   Updated: 2019/08/20 11:05:23 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** is_opt checks if the option opt matches with the argument
** ref passed as a reference value.
*/

int					is_opt(long int opt, long int ref)
{
	return ((opt & ref) == ref) ? 1 : 0;
}

void				ls_option(long int *res, const char *option)
{
	unsigned int	index;

	index = 0;
	while (option[index])
	{
		if (option[index] == 'l')
			*res |= L_OPT;
		else if (option[index] == 'R')
			*res |= R_OPT;
		else if (option[index] == 'r')
			*res |= REV_OPT;
		else if (option[index] == 'a')
			*res |= A_OPT;
		else if (option[index] == 't')
			*res |= T_OPT;
		index++;
	}
}

/*
** ft_getopt get option specified in opt_str string and store the result in a
** long int where each bit correspond to a unique option. The f function define
** how you store the option in the long int. Return 0 if there is no option, -1
** if an error occurs, the ascii code if the option isn't in opt_str, the long
** int mask must be set from 2^8 in order to avoid confusion with ascii code.
*/

static int			is_valid_opt(const char c, const char *opt_str)
{
	unsigned int	index;

	index = 0;
	if (!c)
		return (0);
	while (opt_str[index])
		if (c == opt_str[index++])
			return (1);
	return (0);
}

long int			ft_getopt(const int ac, char **av, const char *opt_str,
		void (f)(long int*, const char*))
{
	unsigned int	i;
	unsigned int	j;
	long int		res;

	i = 0;
	res = 0;
	if (!av || ac <= 1 || !opt_str)
		return (0);
	while (++i < (unsigned int)ac && av[i])
	{
		if (av[i][0] != '-' || ft_strlen(av[i]) < 2)
			return (res);
		if (!ft_strncmp(av[i], "--", 2))
			return (res);
		j = 1;
		while (av[i][j])
		{
			if (is_valid_opt(av[i][j], opt_str))
				f(&res, &av[i][j]);
			else
				return (res);
			j++;
		}
	}
	return (res);
}

int					check_opt_error(int ac, char **av, char *opt_str,
		char *bad_opt)
{
	int	i;
	int	j;

	if (!av || ac <= 1 || !opt_str)
		return (0);
	i = 1;
	while (i < ac)
	{
		if (av[i][0] != '-' || !ft_strcmp(av[i], "--") || ft_strlen(av[i]) < 2)
			return (0);
		if (av[i][1] == '-' && av[i][0] == '-' && av[i][2])
			return (-1);
		j = 1;
		while (av[i][j])
		{
			if (!is_valid_opt(av[i][j], opt_str))
			{
				*bad_opt = av[i][j];
				return (-2);
			}
			j++;
		}
		i++;
	}
	return (0);
}
