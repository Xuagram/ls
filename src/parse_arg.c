/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/31 11:30:15 by temehenn          #+#    #+#             */
/*   Updated: 2019/08/20 11:06:59 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static char		**get_argv_arg(char **argv, int argc, int pos_arg)
{
	char	**new;
	int		i;
	size_t	size;

	i = 0;
	size = argc - pos_arg + 1;
	if (!(new = ft_memalloc(size * sizeof(char *))))
		return (NULL);
	pos_arg = (pos_arg == 0) ? 1 : pos_arg;
	while (pos_arg < argc)
	{
		if (!(new[i++] = ft_strdup(argv[pos_arg++])))
			return (NULL);
	}
	return (new);
}

static t_list	*fill_arg_lst(char *arg, t_list *lst, int lnk)
{
	t_list	*new;
	t_file	elem;

	new = NULL;
	ft_bzero(&elem, sizeof(t_file));
	if (lnk)
		lstat(arg, &elem.data);
	else
		stat(arg, &elem.data);
	ft_strcpy(elem.d_name, arg);
	if (!(new = ft_lstnew(&elem, sizeof(t_file))))
		return (NULL);
	ft_lstadd(&lst, new);
	return (lst);
}

static int		get_arg_error(char **arg, t_list **error)
{
	int			i;
	struct stat check;

	i = 0;
	while (arg[i])
	{
		if (lstat(arg[i], &check) == -1)
		{
			if (!(*error = fill_arg_lst(arg[i], *error, 0)))
				return (-1);
			ft_strclr(arg[i]);
		}
		i++;
	}
	return (0);
}

static int		get_valid_arg(char **arg, t_list **file, t_list **dir, int opt)
{
	int			i;
	struct stat	check;

	i = -1;
	while (arg[++i])
	{
		if (arg[i][0] && lstat(arg[i], &check) == -1)
			return (-1);
		if (arg[i][0] && (S_ISDIR(check.st_mode)
				|| is_slnk_dir(arg[i], check, opt)))
		{
			if (!(*dir = fill_arg_lst(arg[i], *dir, 0)))
				return (-1);
		}
		else if (arg[i][0] && S_ISLNK(check.st_mode) && is_opt(opt, L_OPT))
		{
			if (!(*file = fill_arg_lst(arg[i], *file, 1)))
				return (-1);
		}
		else if (arg[i][0])
			if (!(*file = fill_arg_lst(arg[i], *file, 0)))
				return (-1);
	}
	return (0);
}

/*
** parse_args parses the arguments of ls. First it skips
** the options. Then it builds a node of the list and adds
** it to one of the three lists according to the nature of
** the argument. There is one list for the errors, one for
** the files and one for the directories. It calls the
** function that will sort each of the list
*/

int				parse_args(int argc, char **argv, t_all_list *arg, int pos_arg)
{
	char	**get_arg;

	if (!(get_arg = get_argv_arg(argv, argc, pos_arg)))
		return (-1);
	if (get_arg_error(get_arg, &(arg->error)) == -1)
		return (-1);
	if (get_valid_arg(get_arg, &(arg->file), &(arg->dir), arg->opt) == -1)
		return (-1);
	if (arg->error && !(arg->error = sort_list(arg->opt, arg->error, 1)))
		return (-1);
	if (arg->file && !(arg->file = sort_list(arg->opt, arg->file, 0)))
		return (-1);
	if (arg->dir && !(arg->dir = sort_list(arg->opt, arg->dir, 0)))
		return (-1);
	ft_freetab((void **)get_arg);
	free(get_arg);
	return (0);
}
