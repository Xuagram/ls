/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print_arg.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: haffner <haffner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/20 12:26:11 by mahaffne          #+#    #+#             */
/*   Updated: 2020/07/24 22:05:27 by haffner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** print_dir checks if there is more than one element in the
** list of directories to display the name of the directory.
** It will then display the list according to the display option
** sending it through ft_ls function. It then handle the display
** of the carriage return, except for the last element of the list.
*/

static int		print_dir(t_all_list arg, t_list *tmp_err)
{
	int			ret;

	arg.tmp_dir = arg.dir;
	ret = (list_size(arg.tmp_dir) > 1 || tmp_err || arg.file) ? 1 : 0;
	arg.is_arg = 1;
	while (arg.tmp_dir)
	{
		if (ret == 1)
			ft_printf("%s:\n", ((t_file *)arg.tmp_dir->content)->d_name);
		if (ft_ls(((t_file *)arg.tmp_dir->content)->d_name, arg) == -1)
			return (-1);
		if (list_size(arg.tmp_dir) > 1)
			ft_putstr("\n");
		arg.tmp_dir = arg.tmp_dir->next;
	}
	return (0);
}

/*
** print_file manages the display of the file list.
** It first handles the l option : it creates a structure that
** contains all the data needed (rights, last modification date,
** owner, group) in the l option. Then ls_l displays the data.
*/

static int		print_file(t_all_list arg, t_list *tmp)
{
	t_list	*tmp2;

	tmp2 = tmp;
	while (tmp)
	{
		if (is_opt(arg.opt, L_OPT))
		{
			crea_elem("", ((t_file *)tmp->content)->d_name, tmp->content,
					&arg);
			if (ls_l(tmp->content, arg.tab, arg.size_max) == -1)
				return (-1);
			if (list_size(tmp) > 1)
				ft_putendl(NULL);
		}
		else
		{
			ft_printf("%s", ((t_file *)tmp->content)->d_name);
			if (tmp->next || list_size(tmp2) == 1 || !arg.dir)
				ft_putchar('\n');
		}
		tmp = tmp->next;
	}
	return (0);
}

/*
** print_args handles the display of the three list of files.
** It starts with the list of errors, going through the whole
** list and displaying the error message. Then it calls the
** function that display the file list, handles the carriage return
** depending on the types of arguments available. Finally it calls
** the function managing the display of the directories.
*/

static int		print_args(t_all_list arg, t_all_list all)
{
	t_list		*tmp;
	t_list		*tmp_err;

	tmp = arg.file;
	tmp_err = arg.error;
	while (arg.error)
	{
		ft_putstr_fd("ls: ", 2);
		ft_putstr_fd(((t_file *)arg.error->content)->d_name, 2);
		ft_putstr_fd(": No such file or directory\n", 2);
		arg.error = arg.error->next;
	}
	if (print_file(arg, tmp) == -1)
		return (-1);
	if (arg.file && arg.dir && !is_opt(all.opt, L_OPT))
		ft_putstr("\n\n");
	else if (arg.file && arg.dir && is_opt(all.opt, L_OPT))
		ft_putstr("\n");
	arg.error = tmp_err;
	if (arg.dir && print_dir(arg, tmp_err) == -1)
		return (-1);
	return (0);
}

/*
** manage_args calls the function that handle the display of the
** different lists. If there is any error returned by this function,
** print_error is also called.
*/

int				manage_args(t_all_list all, t_all_list arg)
{
	if (print_args(arg, all) < 0)
	{
		print_error(arg, NULL);
		free_arg(&arg);
		return (-1);
	}
	return (0);
}
