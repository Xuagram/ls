/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_func.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/19 16:48:57 by teddy             #+#    #+#             */
/*   Updated: 2019/08/03 16:02:12 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** list_size get the size of the list in order to be able to
** split the list in two.
*/

int		list_size(t_list *lst)
{
	int size;

	size = 0;
	while (lst)
	{
		size++;
		lst = lst->next;
	}
	return (size);
}

/*
** set_list numerates the order of the elements of the list.
*/

void	set_list(t_list *lst)
{
	int		count;
	t_list	*tmp;

	count = 1;
	tmp = lst;
	while (tmp)
	{
		if ((t_file*)(tmp)->content)
		{
			((t_file*)(tmp)->content)->rank = count;
			count++;
		}
		tmp = tmp->next;
	}
}

/*
** free_lst is a function that free the list and the content
** of all its elements.
*/

void	free_lst(t_list **lst)
{
	t_list *tmp;

	if (!lst)
		return ;
	while (*lst)
	{
		tmp = *lst;
		*lst = (*lst)->next;
		free(tmp->content);
		free(tmp);
		tmp = NULL;
	}
	*lst = NULL;
}

void	free_all(t_all_list *all)
{
	if (all)
	{
		free_lst(&(all->file));
		free_lst(&(all->tmp_dir));
	}
}

void	free_arg(t_all_list *arg)
{
	free_lst(&(arg->dir));
	free_lst(&(arg->file));
	free_lst(&(arg->error));
}
