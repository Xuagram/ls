/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/14 17:26:36 by teddy             #+#    #+#             */
/*   Updated: 2019/08/20 11:03:24 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

int		get_name(char *name)
{
	int i;

	i = 0;
	while (name[i] && name[i] != '/')
		i++;
	if (!name[i])
		return (0);
	i = 0;
	while (name[i])
		i++;
	if (i)
		i--;
	while (name[i] != '/')
		i--;
	i++;
	return (i);
}

void	print_error(t_all_list all, char *path)
{
	if (!all.is_arg)
		ft_printf("ls: %s: ", path + get_name(path));
	else
	{
		if (all.tmp_dir)
		{
			ft_printf("ls: %s: ", ((t_file *)all.tmp_dir->content)->d_name
				+ get_name(((t_file *)all.tmp_dir->content)->d_name));
		}
	}
	perror(NULL);
}
