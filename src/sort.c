/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/12 14:36:56 by mahaffne          #+#    #+#             */
/*   Updated: 2019/08/20 11:10:13 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** Compares two strings in order to sort them according
** to the file's name ascii value in an ascending sort.
*/

static int	ascii_sort(t_file *left, t_file *right)
{
	if (ft_strcmp(left->d_name, right->d_name) <= 0)
		return (1);
	return (0);
}

/*
** Compares two strings in order to sort them according
** to the file's name ascii value in a descending sort.
*/

static int	reverse_ascii(t_file *left, t_file *right)
{
	if (ft_strcmp(left->d_name, right->d_name) >= 0)
		return (1);
	return (0);
}

/*
** Compares two strings in order to sort them according
** to the time of the last modification in a ascending sort.
*/

static int	time_sort(t_file *left, t_file *right)
{
	if (left->data.st_mtime > right->data.st_mtime)
		return (1);
	return (0);
}

/*
** Manages the application of the different sorting function
** according to the corresponding option given (none for ascii,
** -r for reverse ascii, -t for time and -t -r for reverse time).
*/

t_list		*sort_list(long int opt, t_list *lst, int err)
{
	if (!(lst = merge_sort(lst, reverse_ascii)))
		return (0);
	if (is_opt(opt, T_OPT))
	{
		if (!(lst = merge_sort(lst, time_sort)))
			return (0);
	}
	else
	{
		if (!(lst = merge_sort(lst, ascii_sort)))
			return (0);
	}
	if (is_opt(opt, REV_OPT) && !err)
		ft_lst_reverse(&lst);
	return (lst);
}
