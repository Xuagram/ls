/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/03 15:53:38 by mahaffne          #+#    #+#             */
/*   Updated: 2019/08/20 11:06:09 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** Point_dir checks if the file is the current or parent directory
** Upon the file is nammed is a '.' or '..' the function returns 1.
*/

int			point_dir(char *name)
{
	return (!ft_strcmp(name, ".") || !ft_strcmp(name, "..") ? 1 : 0);
}

/*
** It also gets the maximum value for some data in order to
** find the appropriate padding for the display of the option
** -l.
*/

static void	get_max_padding(t_file *elem, unsigned int size_max[8])
{
	if (S_ISBLK(elem->data.st_mode) || S_ISCHR(elem->data.st_mode))
	{
		if (get_size(major(elem->data.st_rdev)) > size_max[6])
			size_max[6] = get_size(major(elem->data.st_rdev));
		if (get_size(minor(elem->data.st_rdev)) > size_max[7])
			size_max[7] = get_size(minor(elem->data.st_rdev));
	}
	size_max[5] = size_max[5] + elem->data.st_blocks;
	if (get_size((unsigned int)elem->data.st_nlink) > size_max[1])
		size_max[1] = get_size((unsigned int)elem->data.st_nlink);
	if (ft_strlen(elem->uid) > size_max[2])
		size_max[2] = ft_strlen(elem->uid);
	if (ft_strlen(elem->gid) > size_max[3])
		size_max[3] = ft_strlen(elem->gid);
	if (get_size((unsigned int)elem->data.st_size) > size_max[4])
		size_max[4] = get_size((unsigned int)elem->data.st_size);
}

/*
** the function create_elem initializes the structure elem
** which is the node that will compound the chained list.
*/

int			crea_elem(char *path, char *d_name, t_file *elem,
			t_all_list *all)
{
	if (path && *path && d_name && d_name[0] != '/')
		ft_strcat(ft_strcat(ft_strcpy(elem->path, path), "/"), d_name);
	else if (path && *path && d_name[0] == '/')
		ft_strcat(ft_strcat(ft_strcpy(elem->path, path), ""), d_name);
	else
		ft_strcpy(elem->path, d_name);
	ft_strcpy(elem->d_name, d_name);
	if (lstat(elem->path, &elem->data))
		return (-1);
	if (S_ISLNK(elem->data.st_mode && !is_opt(all->opt, L_OPT)))
		if (stat(elem->path, &elem->data))
			return (-1);
	if (!(elem->uid = get_uid(elem->data.st_uid)))
		elem->uid = ft_itoa(elem->data.st_uid);
	if (!(elem->gid = get_gid(elem->data.st_gid)))
		elem->uid = ft_itoa(elem->data.st_gid);
	if (!ft_strcpy(elem->date, ctime(&elem->data.st_mtime)))
		elem->uid = ft_itoa(elem->data.st_gid);
	get_max_padding(elem, all->size_max);
	return (0);
}

/*
** The function print_ls is meant to display the content
** of the list 'file'.
*/

int			print_ls(t_all_list all, long int opt, long int tab[9][2],
		unsigned int size_max[8])
{
	if (all.file)
	{
		if (is_opt(opt, A_OPT) && !is_opt(opt, L_OPT))
			ls_a(all.file);
		else if (is_opt(opt, L_OPT))
		{
			if (ls_la(all.file, opt, tab, size_max) == -1)
				return (-1);
		}
		else
			ls(all.file);
	}
	return (0);
}

/*
** The function 'ls' initializes the lists, and calls itself to get
** recursively into all the directories found.
*/

int			ft_ls(char *path, t_all_list all)
{
	init_list(&all);
	ft_bzero(all.size_max, 8 * sizeof(int));
	if (open_directory(path, &all) == -1)
		print_error(all, path);
	if (print_ls(all, all.opt, all.tab, all.size_max) == -1)
		print_error(all, path);
	while (all.dir && is_opt(all.opt, R_OPT))
	{
		ft_printf("\n%s:\n", ((t_file *)all.dir->content)->path);
		if (!point_dir(((t_file *)all.dir->content)->d_name)
				&& !is_opt(all.opt, A_OPT))
			ft_ls(((t_file *)all.dir->content)->path, all);
		else if (!point_dir(((t_file *)all.dir->content)->d_name)
				&& is_opt(all.opt, A_OPT))
			ft_ls(((t_file *)all.dir->content)->path, all);
		all.dir = all.dir->next;
	}
	free_all(&all);
	return (0);
}
