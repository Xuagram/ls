/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_list.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/01 17:59:55 by mahaffne          #+#    #+#             */
/*   Updated: 2019/08/20 11:00:13 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

static int		sort(t_all_list *all)
{
	if (all->dir)
	{
		if (!(all->dir = sort_list(all->opt, all->dir, 0)))
			return (-1);
	}
	if (all->file)
		if (!(all->file = sort_list(all->opt, all->file, 0)))
			return (-1);
	all->tmp_dir = all->dir;
	return (0);
}

static int		get_elem(char *path, char *name, t_file *elem, t_all_list *all)
{
	if (is_opt(all->opt, A_OPT))
	{
		if (crea_elem(path, name, elem, all) == -1)
			return (-1);
	}
	else if (!point_dir(name) && name[0] != '.')
	{
		if (crea_elem(path, name, elem, all) < 0)
			return (-1);
	}
	return (0);
}

/*
** The function build_list opens the directory and build two
** lists according to the format of the file. There is a chained
** list 'file' that registers every file while the second chained
** list nammed 'dir' registers only the directories.
*/

static int		build_list(t_all_list *all, char *name, t_file *elem)
{
	t_list		*lst;
	t_list		*lst2;

	if (is_opt(all->opt, A_OPT) || (!is_opt(all->opt, A_OPT)
				&& name[0] != '.'))
	{
		if (!(lst2 = ft_lstnew(elem, sizeof(t_file))))
			return (-1);
		if (!point_dir(elem->d_name) && ((S_ISDIR(elem->data.st_mode)
						&& name[0] != '.') || (S_ISDIR(elem->data.st_mode)
							&& (is_opt(all->opt, A_OPT)))))
		{
			if (!(lst = ft_lstnew(elem, sizeof(t_file))))
				return (-1);
			ft_lstadd(&all->dir, lst);
		}
		ft_lstadd(&all->file, lst2);
	}
	return (0);
}

/*
** Open_directory opens the directory that has been sent.
** it then looks up all the files contained in the directory
** with readdir. For each file it creates a structure with
** get_elem, with information regarding this file needed for ls -l.
** It then puts this information in lists in build_list.
** Finally it closes the directory and sorts the list.
*/

int				open_directory(char *path, t_all_list *all)
{
	t_file		elem;
	t_dir		dir;

	if (!(dir.dir = opendir(path)))
	{
		ft_putstr_fd("ls: ", 2);
		ft_putstr_fd(path + get_name(path), 2);
		ft_putstr_fd(": ", 2);
		return (-1);
	}
	while ((dir.pdirent = readdir(dir.dir)) != NULL)
	{
		if (get_elem(path, dir.pdirent->d_name, &elem, all) < 0)
			return (-1);
		if (build_list(all, dir.pdirent->d_name, &elem) < 0)
			return (-1);
	}
	closedir(dir.dir);
	sort(all);
	return (0);
}
