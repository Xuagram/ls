/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/04 13:22:09 by teddy             #+#    #+#             */
/*   Updated: 2019/08/03 15:59:30 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

unsigned int	get_size(long long unsigned int nb)
{
	unsigned int count;

	count = 0;
	while (nb)
	{
		nb /= 10;
		count++;
	}
	return ((count) ? count : 1);
}

void			print_list(t_list *lst)
{
	t_list *tmp;

	if (!lst)
	{
		ft_printf("La liste est vide");
	}
	tmp = lst;
	while (tmp)
	{
		ft_putendl(((t_file *)tmp->content)->d_name);
		tmp = tmp->next;
	}
}

/*
** init_struct initializes the lists of all and arg,
** the array of rights and the maximum value of the
** name of file.
*/

void			init_struct(t_all_list *all, t_all_list *arg)
{
	ft_bzero(all, sizeof(t_all_list));
	ft_bzero(arg, sizeof(t_all_list));
	init_rights(arg->tab);
	init_rights(all->tab);
}

/*
** init_list initializes to NULL the lists of
** error, file and dir containing the errors,
** files and directories.
*/

void			init_list(t_all_list *all)
{
	all->error = NULL;
	all->file = NULL;
	all->dir = NULL;
	all->tmp_dir = NULL;
}
