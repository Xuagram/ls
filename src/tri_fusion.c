/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tri_fusion.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/29 11:57:05 by mahaffne          #+#    #+#             */
/*   Updated: 2019/08/15 15:53:31 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_ls.h"

/*
** fill_result adds the sorted element in the final list 'result'.
*/

static t_list	*fill_result(t_list **result, t_list *lst)
{
	t_list		*tmp;

	tmp = 0;
	if (!(tmp = ft_lstnew(lst->content, sizeof(t_file))))
		return (0);
	ft_lst_push_back(result, tmp);
	return (lst);
}

/*
** append_list appends the other element (either right or left
** depending on the previous element that has been added) to
** the final list result.
*/

static t_list	*append_list(t_list *left, t_list *right, t_list *result)
{
	while (left)
	{
		if (!(left = fill_result(&result, left)))
			return (0);
		left = left->next;
	}
	while (right)
	{
		if (!(right = fill_result(&result, right)))
			return (0);
		right = right->next;
	}
	return (result);
}

/*
** merge is used to sort the left group of elements with the
** right group of elements.
*/

static t_list	*merge(t_list *left, t_list *right, int (f)(t_file *, t_file *))
{
	t_list		*result;
	t_list		*b_list[2];

	result = NULL;
	b_list[0] = left;
	b_list[1] = right;
	while (left && right)
	{
		if (f(left->content, right->content))
		{
			if (!(left = fill_result(&result, left)))
				return (NULL);
			left = left->next;
		}
		else
		{
			if (!(right = fill_result(&result, right)))
				return (NULL);
			right = right->next;
		}
	}
	result = append_list(left, right, result);
	free_lst(&b_list[0]);
	free_lst(&b_list[1]);
	return (result);
}

/*
** merge_R sets the second half of the group of elements.
** It calls in a recursive way merge_sort until the groups
** are constituted by only one isolated element.
*/

static int		merge_r(t_list *list[2], t_list *b_left, t_list *lst,
		int (f)(t_file*, t_file*))
{
	list[1] = lst->next;
	list[0]->next = NULL;
	list[0] = b_left;
	if (!(list[0] = merge_sort(list[0], f)))
		return (-1);
	if (!(list[1] = merge_sort(list[1], f)))
		return (-1);
	return (0);
}

/*
** Merge_sort is the core function. It sets the list, divides into two
** groups the list, according the middle of list, calculating it for even
** and odd numbers of elements. It then calls merge function that is joining
** the two different groups according to the type of sort given.
*/

t_list			*merge_sort(t_list *lst, int (f)(t_file*, t_file*))
{
	t_list	*list[2];
	t_list	*b_left;
	t_list	*result;
	int		mid;

	list[0] = lst;
	b_left = list[0];
	set_list(lst);
	if ((mid = list_size(lst)) <= 1)
		return (lst);
	else
	{
		mid = (mid % 2) ? (mid / 2) + 1 : mid / 2;
		while (lst && ((t_file*)lst->content)->rank < mid)
		{
			lst = lst->next;
			list[0] = list[0]->next;
		}
		if (merge_r(list, b_left, lst, f) == -1)
			return (0);
		if (!(result = merge(list[0], list[1], f)))
			return (0);
		return (result);
	}
}
