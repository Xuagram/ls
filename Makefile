# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/04/15 13:17:50 by temehenn          #+#    #+#              #
#    Updated: 2019/08/16 15:59:49 by temehenn         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=ft_ls
CC=gcc
LIBFTDIR= ./libft
SRCDIR = src
OBJDIR = obj
FLAGS= -Wall -Wextra -Werror -g
SRCFILE = 	build_list.c\
			error.c\
			ft_getopt.c\
			init.c\
			list_func.c\
			ls_l.c\
			ft_ls.c\
			main.c\
			maj_min.c\
			print_ls.c\
			rights.c\
			sort.c\
			slnk_dir.c\
			tri_fusion.c\
			parsing.c\
			print_arg.c\
			parse_arg.c\

SRC = $(addprefix $(SRCDIR), $(SRCFILE))
OBJFILE = $(SRCFILE:.c=.o)
OBJ = $(addprefix $(OBJDIR)/,$(OBJFILE))

all: $(NAME)

$(NAME): $(OBJ)
	@$(CC)  -o $(NAME) $(OBJ) ./libft/libft.a
	@echo "\033[32mCompilation SUCCEED. Binary created :\033[32;1m" $@

$(OBJ): | $(OBJDIR)

$(OBJDIR):
	@make -C libft/
	@mkdir $@

$(OBJDIR)/%.o :$(SRCDIR)/%.c
	@$(CC) $(FLAGS) -o $@ -c $< -I include/ -I $(LIBFTDIR)
	@echo "File compiled : " $<

clean:
	@make -C libft/ clean
	@rm -rf $(OBJDIR)
	@echo "\033[36mObjects have been deleted.\033[00m"

fclean: clean
	@make -C libft/ fclean
	@rm -f $(NAME)
	@echo "\033[36m"$(NAME) "have been deleted.\033[00m"

re: fclean all

.PHONY: all clean fclean re
