/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ls.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: haffner <haffner@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/28 11:23:28 by mahaffne          #+#    #+#             */
/*   Updated: 2020/07/24 22:04:17 by haffner          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_LS_H
# define FT_LS_H

# include <sys/types.h>
# include <sys/stat.h>
# include <unistd.h>
# include <limits.h>
# include <stdlib.h>
# include <pwd.h>
# include <grp.h>
# include <time.h>
# include <string.h>
# include <errno.h>
# include <dirent.h>
 #include <sys/sysmacros.h>
# include "libft.h"
# include "ft_printf.h"

# define R_OPT  1L << 10
# define L_OPT  1L << 11
# define REV_OPT  1L << 12
# define T_OPT  1L << 13
# define A_OPT  1L << 14
# define ERR_OPT 1L << 63

typedef struct		s_file
{
	char			d_name[256];
	struct stat		data;
	char			*uid;
	char			*gid;
	char			path[4096];
	char			date[26];
	int				rank;
}					t_file;

typedef struct		s_dir
{
	DIR				*dir;
	struct dirent	*pdirent;
}					t_dir;

typedef struct		s_argu
{
	int				i;
	int				data;
	struct s_argu	*next;
}					t_argu;

typedef struct		s_all_list
{
	long int		opt;
	long int		tab[9][2];
	unsigned int	size_max[8];
	int				is_arg;
	t_list			*error;
	t_list			*file;
	t_list			*dir;
	t_list			*tmp_dir;
}					t_all_list;

/*
** init.c
*/
unsigned int		get_size(long long unsigned int nb);
void				init_list(t_all_list *all);
void				init_struct(t_all_list *all, t_all_list *arg);
void				print_list(t_list *lst);

/*
** slnk_dir.c
*/
int					is_slnk_dir(char *name, struct stat lnk, int opt);

/*
** parsing.c
*/
int					parsing(int argc, char **argv, t_all_list *all,
					t_all_list *arg);

/*
** ft_getopt.c
*/
int					is_opt(long int opt, long int ref);
void				ls_option(long int *res, const char *option);
long int			ft_getopt(const int ac, char **av, const char *opt_str,
					void (f)(long int*, const char*));
int					check_opt_error(int ac, char **av, char *opt_str,
		char *bad_opt);

/*
** parse_arg.c
*/
int					parse_args(int argc, char **argv, t_all_list *arg,
					int pos_arg);

/*
** sort.c
*/
t_list				*sort_list(long int opt, t_list *lst, int err);

/*
** tri_fusion.c
*/
t_list				*merge_sort(t_list *lst, int (f)(t_file *, t_file *));

/*
** print_arg.c
*/
int					manage_args(t_all_list all, t_all_list arg);

/*
** ft_ls.c
*/
int					point_dir(char *name);
int					crea_elem(char *path, char *d_name,
					t_file *elem, t_all_list *all);
int					ft_ls(char *path, t_all_list all);

/*
** build_list.c
*/
int					open_directory(char *path, t_all_list *all);

/*
** list_func.c
*/
void				set_list(t_list *lst);
int					list_size(t_list *lst);
void				free_all(t_all_list *all);
void				free_arg(t_all_list *arg);
void				free_lst(t_list **lst);

/*
** ls_l.c
*/
char				*get_gid(uid_t gid);
char				*get_uid(uid_t uid);
int					get_slnk(char *path, struct stat sb);
int					get_time(char str[26], struct stat sb);

/*
** maj_min.c
*/
void				put_maj_min(dev_t dev, unsigned int size_max[8]);
void				put_space_size(int size, unsigned int size_max[8]);

/*
** rights.c
*/
void				init_rights(long int tab[9][2]);
void				rights(long int tab[9][2], struct stat sb);

/*
** print_ls.c
*/
void				ls(t_list *file);
void				ls_a(t_list *file);
int					ls_l(t_file *ptr, long int tab[9][2],
					unsigned int size_max[8]);
int					ls_la(t_list *file, long int opt, long int tab[9][2],
					unsigned int size_max[8]);
void				print_space(unsigned int size_max, unsigned int size_name);

/*
** error.c
*/
void				print_error(t_all_list all, char *path);
int					get_name(char *name);

#endif
