/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_freetab.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/22 10:15:57 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 18:06:53 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

void	**ft_freetab(void **tab)
{
	if (!tab)
		return (NULL);
	while (*tab)
	{
		ft_memdel((void **)&(*tab));
		tab++;
	}
	tab = NULL;
	return (tab);
}
