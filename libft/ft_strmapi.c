/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 11:21:10 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/22 15:20:32 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <stdlib.h>

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char			*str;
	char			*tmp_str;
	unsigned int	i;

	str = NULL;
	tmp_str = NULL;
	i = 0;
	if (s)
	{
		if (!(str = ft_strnew(ft_strlen(s))))
			return (NULL);
		tmp_str = str;
		while (s[i])
		{
			*str = f(i, s[i]);
			i++;
			str++;
		}
	}
	return (tmp_str);
}

/*
**static char	ft_next(char c)
**{
**	c = c + 1;
**	return (c);
**}
**
**static int	main(int argc, char **argv)
**{
**	ft_putstr(ft_strmap(argv[1], &ft_next));
**	return (0);
**}
*/
