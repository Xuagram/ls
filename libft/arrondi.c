/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   arrondi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/01 12:30:12 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			modulo(long double a, unsigned int mod)
{
	int res;
	int div;

	res = 0;
	div = a / mod;
	res = a - (div * mod);
	return (res);
}

int			r_power(int nb, int power)
{
	if (power == 0)
		return (1);
	if (power == 1)
		return (nb);
	if (power < 0)
		return (0);
	else
		return (nb * r_power(nb, power - 1));
}

long double	arrondi(long double nb, long int prec)
{
	while (nb > r_power(10, prec))
	{
		if (modulo(nb, 10) > 4)
		{
			nb /= 10;
			nb++;
		}
		else
			nb /= 10;
	}
	return (nb);
}

long double	get_point(long double nb, int len_e, long int prec)
{
	if (!nb)
		return (0);
	else
		nb = arrondi(nb, prec + len_e);
	return (nb);
}
