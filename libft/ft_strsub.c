/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/09 18:42:03 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/25 14:13:10 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>
#include "libft.h"

char	*ft_strsub(char const *s, unsigned int start, size_t len)
{
	char *bout;

	if (!s || !(bout = ft_strnew(len)))
		return (NULL);
	bout = ft_strncpy(bout, s + start, len);
	return (bout);
}
