/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 10:01:39 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 12:31:46 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"
#include <stdio.h>

char	*ft_strdup(const char *s1)
{
	char	*new;
	size_t	size;

	new = 0;
	size = ft_strlen(s1);
	if (!(new = ft_memalloc(size + 1)))
		return (0);
	new = ft_memcpy(new, s1, size);
	return (new);
}
/*
**int	main(int argc, char **argv)
**{
**	ft_strdup(argv[1]);
**	return (0);
**}
*/
