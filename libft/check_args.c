/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_args.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/04/21 13:32:02 by temehenn          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int	is_signed_conv(char c)
{
	return (c == 'i' || c == 'd' || c == 'f') ? 1 : 0;
}

int	is_unsigned_conv(char c)
{
	return (c == 'o' || c == 'u' || c == 'x' || c == 'X') ? 1 : 0;
}

int	is_flag(char c)
{
	return (c == '#' || c == ' ' || c == '0' || c == '+' || c == '-');
}

int	is_len_flag(char c, char d)
{
	return (c == 'h' || (c == 'h' && d == 'h')
			|| c == 'l' || (c == 'l' && d == 'l') || c == 'L') ? 1 : 0;
}

int	is_conv(char c, char d)
{
	return (!is_len_flag(c, d) && !is_flag(c) && c != '.' && !ft_isdigit(c)
			&& ft_isalpha(c)) ? 1 : 0;
}
