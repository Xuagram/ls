/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_reverse.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 18:42:40 by mahaffne          #+#    #+#             */
/*   Updated: 2019/08/15 16:05:47 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lst_reverse(t_list **blst)
{
	t_list	*current;
	t_list	*next;
	t_list	*prev;

	if (!blst)
		return ;
	current = *blst;
	next = current->next;
	prev = current;
	current->next = NULL;
	while (next)
	{
		prev = current;
		current = next;
		next = current->next;
		current->next = prev;
	}
	*blst = current;
}

/*
**static int	main(int argc, char **argv)
**{
**	t_list *lst = ft_lstnew(argv[1], sizeof(argv[1]));
**	ft_lst_push_back(&lst, ft_lstnew(argv[2], sizeof(argv[2])));
**	ft_lst_push_back(&lst, ft_lstnew(argv[3], sizeof(argv[3])));
**	ft_lst_push_back(&lst, ft_lstnew(argv[4], sizeof(argv[4])));
**	ft_lst_push_back(&lst, ft_lstnew(argv[5], sizeof(argv[5])));
**	ft_lst_reverse(&lst);
**	ft_lstiter(lst, ft_lstprint);
**	return (0);
**}
*/
