/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   core.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/05/17 19:20:12 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:48:14 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void	reset_arg(t_arg *arg)
{
	arg->null = 0;
	arg->conv = 0;
	ft_bzero(arg->len, 3);
	arg->sharp = 0;
	arg->space = 0;
	arg->zero = 0;
	arg->plus = 0;
	arg->less = 0;
	arg->prec = -1;
	arg->field = 0;
}

int		core(char *format, va_list ap)
{
	int		ret;

	if (!format)
		return (-1);
	ret = 0;
	while (*format)
	{
		if (*format == '%' && *(format + 1) != '%')
			manage_arg(&format, ap, &ret);
		else if (*format == '%' && *(format + 1) == '%')
		{
			format = format + 1;
			ft_putchar('%');
			ret++;
		}
		else
		{
			write(1, format, 1);
			ret++;
		}
		if (*format)
			format++;
	}
	return (ret);
}
