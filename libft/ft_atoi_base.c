/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi_base.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/21 11:03:46 by mahaffne          #+#    #+#             */
/*   Updated: 2018/11/25 13:59:16 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int		ft_check_base(char *base)
{
	int i;
	int j;

	i = 0;
	if (base[i])
	{
		while (base[i] != '\0')
		{
			j = i + 1;
			while (base[j] != '\0')
			{
				if ((base[j] == base[i]) || (base[i] == '+')
						|| (base[i] == '-') || (base[i] == ' ')
						|| (base[i] >= 9 && base[i] <= 13))
					return (0);
				j++;
			}
			i++;
		}
		if (i == 1)
			return (0);
		return (i);
	}
	return (0);
}

static int		ft_check_neg(char *str, int *neg)
{
	int i;
	int k;

	i = 0;
	while ((str[i] > 8 && str[i] < 14) || str[i] == 32)
		i++;
	if (str[i] == '-')
		*neg = -1;
	if (str[i] == '+' || str[i] == '-')
		i++;
	k = i;
	return (k);
}

static int		ft_conv(char *str, char *base, int i)
{
	int j;

	j = 0;
	while (str[i] != base[j])
		j++;
	return (j);
}

int				ft_atoi_base(char *str, char *base)
{
	int len_b;
	int len_s;
	int i;
	int nb;
	int neg;

	neg = 1;
	i = ft_check_neg(str, &neg);
	nb = 0;
	len_b = ft_check_base(base);
	len_s = ft_strlen(str);
	if (len_b)
	{
		nb = ft_conv(str, base, i);
		while (++i < len_s && nb <= 2147483647)
		{
			nb = (nb * len_b) + ft_conv(str, base, i);
		}
		return (neg * nb);
	}
	return (0);
}

/*
**int	main(int argc, char **argv)
**{
**	ft_putnbr(ft_atoi_base(argv[1], argv[2]));
**	return (0);
**}
*/
