/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstdel.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 15:08:32 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:47:40 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <string.h>

void	ft_lstdel(t_list **alst, void (*del)(void *, size_t))
{
	t_list	*tmp;

	while (*alst)
	{
		tmp = *alst;
		ft_lstdelone(&tmp, del);
		*alst = (*alst)->next;
	}
	*alst = NULL;
}

/*
**#include <stdio.h>
**
**int	main(int argc, char **argv)
**{
**	t_list	*yolo;
**
**	yolo = ft_lstnew(argv[1], sizeof(argv[1]));
**	yolo->next = ft_lstnew(argv[2], sizeof(argv[2]));
**	ft_lstdel(&yolo, ft_lst_list);
**	ft_printf("%p \n", yolo);
**	while (yolo)
**	{
**		ft_putendl(yolo->content);
**		ft_printf("%p \n", yolo);
**		ft_lstdel(&yolo,ft_lst_list);
**		ft_printf("%p \n", yolo);
**		yolo = yolo->next;
**	}
**return (0);
**}
*/
