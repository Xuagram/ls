/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strjoin.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: teddy <teddy@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/14 14:11:09 by mahaffne          #+#    #+#             */
/*   Updated: 2019/06/04 11:55:34 by teddy            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strjoin(char const *s1, char const *s2)
{
	unsigned int	size;
	char			*new;

	new = NULL;
	size = 0;
	if (s1 && s2)
	{
		size = ft_strlen(s1) + ft_strlen(s2);
		if (!(new = ft_strnew(size)))
			return (NULL);
		new = ft_strcat(ft_strcpy(new, s1), s2);
	}
	else if (s1 || s2)
	{
		if (s1)
			new = ft_strdup(s1);
		else
			new = ft_strdup(s2);
	}
	return (new);
}
/*
** int	main(void)
** {
**	char str[6] = "Pollo";
**	char str2[8] = "Hermano";
**  ft_putstr(ft_strjoin(str, str2));
**  return (0);
**}
*/
