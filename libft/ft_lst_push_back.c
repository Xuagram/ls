/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lst_push_back.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mahaffne <mahaffne@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 14:51:16 by mahaffne          #+#    #+#             */
/*   Updated: 2019/06/03 18:06:59 by mahaffne         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_lst_push_back(t_list **begin_lst, t_list *elem)
{
	t_list *end_list;

	if (!(begin_lst) || !elem)
		return ;
	end_list = *begin_lst;
	if (!(*begin_lst))
		*begin_lst = elem;
	else
	{
		while (end_list->next)
			end_list = end_list->next;
		end_list->next = elem;
	}
}

/*
**static int	main(int argc, char **argv)
**{
**	t_list *lst;
**
**	lst = ft_lstnew(argv[1], ft_atoi(argv[2]));
**	ft_lst_push_back(&lst, ft_lstnew(argv[3], ft_atoi(argv[4])));
**	//ft_lstiter(lst, ft_lstprint);
**	while (lst)
**	{
**		ft_putendl(lst->content);
**		lst = lst->next;
**	}
**	return (0);
**}
*/
