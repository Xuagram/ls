/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: temehenn <temehenn@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/20 10:21:54 by mahaffne          #+#    #+#             */
/*   Updated: 2019/07/31 15:47:40 by temehenn         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strnstr(const char *haystack, const char *needle, size_t len)
{
	const char	*hs_buff;
	size_t		i;
	size_t		j;

	i = 0;
	hs_buff = NULL;
	if (!(needle[i]))
		return ((char *)haystack);
	while (haystack[i] && i < len)
	{
		if (haystack[i] == needle[0])
		{
			j = 0;
			hs_buff = haystack + i;
			while (haystack[i + j] == needle[j] && i + j < len)
			{
				if (!(needle[j + 1]))
					return ((char *)hs_buff);
				j++;
			}
			hs_buff = 0;
		}
		i++;
	}
	return (NULL);
}

/*
**#include <string.h>
**#include <stdio.h>
**
**int	main(int argc, char **argv)
**{
**		char *s1 = "see FF your FF return FF now FF";
**		char *s2 = "FF";
**		size_t max = strlen(s1);
**	//ft_printf("%s\n", strnstr(NULL, NULL, 5));
**	ft_printf("%s\n", ft_strnstr(NULL, NULL, 5));
**	return (0);
**}
*/
