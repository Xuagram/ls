# ls

This project is an implementation of ls command in C, with the following options : `-l`,`-R`,`-a`,`-r` and `-t`.

This is the first project at 42 school for the UNIX System specialization. 
You can find more details on the subject in this repository in the file : `ft_ls.fr.pdf`

# Credits

Project realized with another team member, temehenn who worked on the parsing of options.

# How to run

This program shell can be compiled on all unix system.

- Clone this repository : 
`git clone https://gitlab.com/Xuagram/ls.git`

- Change directory into the new directory and run make
```
cd ls
make 
```

- Run this command replacing with the arguments you want to try.

`./ft_ls` [-Ralrt] [file ...]


Enjoy !